package com.epam.demo.util.mappers;

import com.epam.demo.DTO.Role;
import com.epam.demo.DTO.User;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class UserMapper implements RowMapper<User> {
    @Override
    public User mapRow(ResultSet resultSet, int i) throws SQLException {
        User user = new User();
        user.setId(resultSet.getInt("idUsers"));
        user.setPhone(resultSet.getString("phone"));
        user.setUsername(resultSet.getString("userName"));
        user.setPassword(resultSet.getString("password"));
        user.setRole(Role.valueOf(resultSet.getString("role")));
        return user;
    }
}
