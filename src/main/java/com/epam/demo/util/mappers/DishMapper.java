package com.epam.demo.util.mappers;

import com.epam.demo.DTO.Dish;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class DishMapper implements RowMapper<Dish> {
    @Override
    public Dish mapRow(ResultSet resultSet, int i) throws SQLException {
        Dish dish = new Dish();
        dish.setId(resultSet.getInt("iddish"));
        dish.setDishname(resultSet.getString("dishName"));
        dish.setPricedish(resultSet.getInt("dishPrice"));
        return dish;
    }
}
