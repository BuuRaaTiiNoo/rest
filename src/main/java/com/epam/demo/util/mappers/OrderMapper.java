package com.epam.demo.util.mappers;

import com.epam.demo.DTO.Order;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class OrderMapper implements RowMapper<Order> {
    @Override
    public Order mapRow(ResultSet resultSet, int i) throws SQLException {
        Order order = new Order();
        order.setId(resultSet.getInt("idOrder"));
        order.setStatMake(resultSet.getBoolean("statMake"));
        order.setStatPay(resultSet.getBoolean("statPay"));
        order.setCostOrder(resultSet.getInt("costOrder"));
        return order;
    }
}
