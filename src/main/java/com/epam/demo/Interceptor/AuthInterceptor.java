package com.epam.demo.Interceptor;

import com.epam.demo.DTO.User;
import com.epam.demo.Manager.SessionUserManagerImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Objects;


@Component
public class AuthInterceptor extends HandlerInterceptorAdapter {

    @Autowired
    private SessionUserManagerImpl sessionUserManagerImpl;

    @Override
    public boolean preHandle(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o) throws Exception {
        User user = sessionUserManagerImpl.getCurrentSessionUser();
        if (Objects.isNull(user)) {
            httpServletResponse.sendRedirect("/login");
            return false;
        }
        httpServletRequest.setAttribute("user", user);
        return true;
    }
}
