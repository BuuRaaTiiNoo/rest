package com.epam.demo.Interceptor;

import com.epam.demo.DTO.Role;
import com.epam.demo.DTO.User;
import com.epam.demo.Manager.SessionUserManagerImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Objects;


@Component
public class AdminInterceptor extends HandlerInterceptorAdapter {

    @Autowired
    private SessionUserManagerImpl sessionUserManagerImpl;

    @Override
    public boolean preHandle(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o) throws Exception {
        User user = sessionUserManagerImpl.getCurrentSessionUser();
        if (Objects.isNull(user) || !user.getRole().equals(Role.ADMIN)) {
            httpServletResponse.sendRedirect("/login");
            return false;
        }
        return true;
    }
}
