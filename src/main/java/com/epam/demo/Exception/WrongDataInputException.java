package com.epam.demo.Exception;

public class WrongDataInputException extends Exception {
    public WrongDataInputException(String message) {
        super(message);
    }
}
