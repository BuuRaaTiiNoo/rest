package com.epam.demo.DTO;

public class Order {
    private long id;
    private boolean statMake;
    private boolean statPay;
    private int costOrder;

    public Order() {
    }

    public Order(boolean statMake, boolean statPay, int cost) {
        this.statMake = statMake;
        this.statPay = statPay;
        this.costOrder = cost;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getCostOrder() {
        return costOrder;
    }

    public void setCostOrder(int costOrder) {
        this.costOrder = costOrder;
    }

    public boolean isStatMake() {
        return statMake;
    }

    public void setStatMake(boolean statMake) {
        this.statMake = statMake;
    }

    public boolean isStatPay() {
        return statPay;
    }

    public void setStatPay(boolean statPay) {
        this.statPay = statPay;
    }

    @Override
    public String toString() {
        return "Order{" +
                "id=" + id +
                ", statMake=" + statMake +
                ", statPay=" + statPay +
                ", costOrder=" + costOrder +
                '}';
    }
}
