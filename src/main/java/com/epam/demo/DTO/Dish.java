package com.epam.demo.DTO;

public class Dish {
    private long id;
    private String dishname;
    private int pricedish;

    public Dish() {
    }

    public Dish(String dishname, int pricedish) {
        this.dishname = dishname;
        this.pricedish = pricedish;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getDishname() {
        return dishname;
    }

    public void setDishname(String dishname) {
        this.dishname = dishname;
    }

    public int getPricedish() {
        return pricedish;
    }

    public void setPricedish(int pricedish) {
        this.pricedish = pricedish;
    }

    @Override
    public String toString() {
        return "Dish{" +
                "id=" + id +
                ", dishname='" + dishname + '\'' +
                ", pricedish=" + pricedish +
                '}';
    }
}
