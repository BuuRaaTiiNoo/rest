package com.epam.demo.DTO;

import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.Size;

public class User {

    private long id;

    @NotEmpty(message = "Не заполнен номер телефона")
    private String phone;

    @Size(min=3, max=10, message = "Длина имени от 3 до 10")
    @NotEmpty(message = "Не заполнено имя")
    private String username;

    @Size(min=5, max=10, message = "Длина пароля от 5 до 10")
    @NotEmpty(message = "Не заполнен пароль")
    private String password;

    private Role role;

    public User() {
    }

    public User(String phone, String username, String password, Role role) {
        this.phone = phone;
        this.username = username;
        this.password = password;
        this.role = role;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", phone='" + phone + '\'' +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", role=" + role +
                '}';
    }
}
