package com.epam.demo.DTO;

public enum Role {
    ROOT,
    ADMIN,
    CLIENT
}
