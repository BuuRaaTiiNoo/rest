package com.epam.demo.Controller;

import com.epam.demo.DTO.Dish;
import com.epam.demo.DTO.Order;
import com.epam.demo.DTO.User;
import com.epam.demo.Manager.SessionUserManagerImpl;
import com.epam.demo.Service.Impl.MenuServiceImpl;
import com.epam.demo.Service.Impl.OrderServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.client.support.HttpRequestWrapper;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static java.awt.SystemColor.menu;

@Controller
public class UserController {

    private final MenuServiceImpl menuService;

    private final OrderServiceImpl orderService;

    private final SessionUserManagerImpl sessionUserManager;

    @Autowired
    public UserController(MenuServiceImpl menuService, OrderServiceImpl orderService, SessionUserManagerImpl sessionUserManager) {
        this.menuService = menuService;
        this.orderService = orderService;
        this.sessionUserManager = sessionUserManager;
    }

    @GetMapping("menu")
    public ModelAndView getMenu(ModelAndView modelAndView) {
        User user = sessionUserManager.getCurrentSessionUser();
        modelAndView.addObject("user", user);

        List<Dish> menu = menuService.getAllDishes();
        modelAndView.addObject("menu", menu);
        modelAndView.setViewName("menu");
        return modelAndView;
    }

    @PostMapping("menu")
    public ModelAndView createOrder(ModelAndView modelAndView, Order order, HttpServletRequest httpServletRequest) {
        User user = sessionUserManager.getCurrentSessionUser();
        modelAndView.addObject("user", user);

        ArrayList<String> arrayList = new ArrayList<>(Arrays.asList(httpServletRequest.getParameterValues("checkbox")));
        order.setStatPay(false);
        order.setStatMake(false);

        List<Dish> dishList = new ArrayList<>();
        for(int i = Integer.parseInt(arrayList.get(0))-1; i < arrayList.size(); i++){
            dishList.add(menuService.getDishById(Integer.parseInt(arrayList.get(i))));
        }
        int cost = 0;
        for (Dish dish : dishList){
            cost += dish.getPricedish();
        }

        order.setCostOrder(cost);

        orderService.createOrder(order, arrayList, (int) user.getId());
        modelAndView.setViewName("menu");
        return modelAndView;
    }

    @GetMapping("myorder")
    public ModelAndView getMyOrders(ModelAndView modelAndView) {
        User user = sessionUserManager.getCurrentSessionUser();
        modelAndView.addObject("user", user);

        List<Order> myorder = orderService.getOrderByIdUser(user.getId());
        modelAndView.addObject("myorder", myorder);
        modelAndView.setViewName("myorder");
        return modelAndView;
    }

    @PostMapping("myorder/{id}")
    public ModelAndView payOrder(@PathVariable("id") long id, ModelAndView modelAndView, Order order) {
        User user = sessionUserManager.getCurrentSessionUser();
        modelAndView.addObject("user", user);

        order = orderService.getOrderById(id);
        order.setStatPay(true);
        orderService.payOrder(order);
        modelAndView.setViewName("redirect:/myorder");
        return modelAndView;
    }
}