package com.epam.demo.Controller;

import com.epam.demo.DTO.Role;
import com.epam.demo.DTO.User;
import com.epam.demo.Exception.WrongDataInputException;
import com.epam.demo.Manager.SessionUserManager;
import com.epam.demo.Service.Impl.UserServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.List;


@Controller
public class AuthenticationController {


    @Autowired
    UserServiceImpl userService;

    @Autowired
    SessionUserManager sessionUserManager;

    @GetMapping("login")
    public ModelAndView login(ModelAndView modelAndView) {
        modelAndView.setViewName("login");
        return modelAndView;
    }

    @PostMapping("login")
    public ModelAndView login(User user, ModelAndView modelAndView) {
        User foundUser = userService.authenticateUser(user);
        if (foundUser == null) {
            modelAndView.addObject("error", "Неверный логин или пароль");
            modelAndView.setViewName("login");
            return modelAndView;
        }

        sessionUserManager.setCurrentSessionUser(foundUser);
        modelAndView.addObject("user", foundUser);
        modelAndView.setViewName("redirect:");
        return modelAndView;
    }

    @GetMapping("registration")
    public ModelAndView registration(ModelAndView modelAndView) {
        modelAndView.setViewName("registration");
        return modelAndView;
    }

    @PostMapping("registration")
    public ModelAndView registration(ModelAndView modelAndView, @Validated User user, BindingResult result) {
        if (result.hasErrors()) {
            List<ObjectError> errors = result.getAllErrors();
            modelAndView.addObject("errors", errors);
            modelAndView.setViewName("registration");
            return modelAndView;
        }
        try {
            user.setRole(Role.CLIENT);
            userService.addUser(user);
        } catch (Exception e) {
            modelAndView.addObject("errors", "Такой пользователь уже существует");
            modelAndView.setViewName("registration");
            return modelAndView;
        }
        modelAndView.setViewName("redirect:login");
        return modelAndView;
    }

    @GetMapping("logout")
    public ModelAndView logout(ModelAndView modelAndView, HttpServletRequest httpServletRequest) {
        httpServletRequest.getSession().invalidate();
        modelAndView.setViewName("redirect:login");
        return modelAndView;
    }
}
