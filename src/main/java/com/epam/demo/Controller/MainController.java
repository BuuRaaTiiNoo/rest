package com.epam.demo.Controller;

import com.epam.demo.DTO.User;
import com.epam.demo.Manager.SessionUserManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class MainController {

    private final SessionUserManager sessionUserManager;

    @Autowired
    public MainController(SessionUserManager sessionUserManager) {
        this.sessionUserManager = sessionUserManager;
    }

    @GetMapping({"/", "/index"})
    public ModelAndView index() {
        User user = sessionUserManager.getCurrentSessionUser();
        return new ModelAndView("index", "user", user);
    }

    @GetMapping("about")
    public ModelAndView about() {
        User user = sessionUserManager.getCurrentSessionUser();
        return new ModelAndView("about", "user", user);
    }

}
