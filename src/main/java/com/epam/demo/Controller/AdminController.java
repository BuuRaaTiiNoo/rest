package com.epam.demo.Controller;

import com.epam.demo.DTO.Dish;
import com.epam.demo.DTO.Order;
import com.epam.demo.DTO.User;
import com.epam.demo.Manager.SessionUserManager;
import com.epam.demo.Service.Impl.MenuServiceImpl;
import com.epam.demo.Service.Impl.OrderServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import java.util.ArrayList;
import java.util.List;

@Controller
public class AdminController {
    @Autowired
    SessionUserManager sessionUserManager;

    @Autowired
    OrderServiceImpl orderService;

    @Autowired
    MenuServiceImpl menuService;

    @GetMapping("admin")
    public ModelAndView admin(ModelAndView modelAndView) {
        User user = sessionUserManager.getCurrentSessionUser();
        modelAndView.addObject("user", user);

        List<Order> orders = orderService.getAllOrders();
        List<Dish> dishes = new ArrayList<>();
        for(Order order : orders){
            dishes = menuService.getAllDishesByOrderId((int)order.getId());
            String atrname = String.valueOf(order.getId()+1);
            modelAndView.addObject(atrname, dishes);
        }

        modelAndView.addObject("orders", orders);
        modelAndView.setViewName("admin");
        return modelAndView;
    }

    @PostMapping("admin/{id}")
    public ModelAndView confirmOrder(@PathVariable("id") long id, ModelAndView modelAndView, Order order) {
        User user = sessionUserManager.getCurrentSessionUser();
        modelAndView.addObject("user", user);

        order = orderService.getOrderById(id);
        order.setStatMake(true);
        orderService.confirmOrder(order);
        modelAndView.setViewName("redirect:/admin");
        return modelAndView;
    }
}
