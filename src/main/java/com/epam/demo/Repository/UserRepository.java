package com.epam.demo.Repository;

import com.epam.demo.DTO.User;

import java.util.List;

public interface UserRepository {

    public User getUserByPhone(String phone);

    public void addUser(User user);

    public void deleteUserById(long id);

    public void deleteUserByPhone(String phone);

    public void updateUser(User user);

    public List<User> getAllUsers();
}
