package com.epam.demo.Repository;

import com.epam.demo.DTO.Dish;

import java.util.List;

public interface MenuRepository {

    public List<Dish> getAllDishesByOrderId(int id);

    public Dish getDishById(long id);

    public void addDish(Dish dish);

    public void deleteDishById(long id);

    public void deleteDishByName(String dishName);

    public void updateDish(Dish dish);

    public List<Dish> getAllDishes();

}
