package com.epam.demo.Repository.Impl;

import com.epam.demo.DTO.Order;
import com.epam.demo.Repository.OrderRepository;
import com.epam.demo.util.mappers.OrderMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.util.ArrayList;
import java.util.List;

@Repository
public class OrderRepositoryImpl implements OrderRepository {

    @Autowired
    private DataSource dataSource;

    @Override
    public Order getOrderById(long id) {
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        return jdbcTemplate.queryForObject("SELECT * FROM mydb.order WHERE idorder = ?", new Object[]{id}, new OrderMapper());
    }

    @Override
    public List<Order> getOrderByIdUser(long id) {
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        return jdbcTemplate.query("SELECT * FROM mydb.order WHERE users_idusers = ?", new Object[]{id}, new OrderMapper());
    }

    @Override
    public void createOrder(Order order, ArrayList<String> arrayList, int id) {
        KeyHolder keyHolder = new GeneratedKeyHolder();
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        NamedParameterJdbcTemplate namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
        SqlParameterSource sqlParameterSource = new MapSqlParameterSource()
                .addValue("StatMake", order.isStatMake())
                .addValue("StatPay", order.isStatPay())
                .addValue("CostOrder", order.getCostOrder())
                .addValue("Id", order.getId())
                .addValue("users_idusers", id);
        namedParameterJdbcTemplate.update(
                "INSERT INTO mydb.order (statMake, statPay, costOrder, users_idusers) VALUES (:StatMake, :StatPay, :CostOrder, :users_idusers);", sqlParameterSource, keyHolder);
        long key = keyHolder.getKey().longValue();
        for (String idDish : arrayList) {
            jdbcTemplate.update("INSERT INTO mydb.dishes_has_order (dishes_iddish, order_idorder) VALUES(?, ?);", idDish, key);
        }
    }

    @Override
    public void deleteOrderById(long id) {
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        jdbcTemplate.update("DELETE mydb.order WHERE idorder = ?", id);
    }

    @Override
    public void deleteOrderByStatus(boolean status) {
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        jdbcTemplate.update("DELETE  mydb.order WHERE statPay = ?", status);
    }

    @Override
    public void updateOrder(Order order) {
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        jdbcTemplate.update("UPDATE mydb.order SET statMake = ?, statPay = ?, costOrder = ? WHERE idorder = ?", new Object[]{order.isStatMake(), order.isStatPay(), order.getCostOrder(), order.getId()});
    }

    @Override
    public List<Order> getAllOrders() {
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        return jdbcTemplate.query("SELECT * FROM mydb.order", new OrderMapper());
    }
}
