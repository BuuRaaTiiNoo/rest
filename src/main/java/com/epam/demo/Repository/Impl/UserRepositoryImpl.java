package com.epam.demo.Repository.Impl;

import com.epam.demo.DTO.User;
import com.epam.demo.Repository.UserRepository;
import com.epam.demo.util.mappers.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.util.List;

@Repository
public class UserRepositoryImpl implements UserRepository {

    @Autowired
    private DataSource dataSource;

    @Override
    public User getUserByPhone(String phone) {
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        return jdbcTemplate.queryForObject("SELECT * FROM mydb.users WHERE phone = ?", new Object[]{phone}, new UserMapper());
    }

    @Override
    public void addUser(User user) {
        NamedParameterJdbcTemplate namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
        KeyHolder keyHolder = new GeneratedKeyHolder();
        SqlParameterSource sqlParameterSource = new MapSqlParameterSource()
                .addValue("id", user.getId())
                .addValue("phone", user.getPhone())
                .addValue("userName", user.getUsername())
                .addValue("password", user.getPassword())
                .addValue("role", user.getRole().toString().toUpperCase());
        namedParameterJdbcTemplate.update("INSERT INTO mydb.users (phone, username, password, role) VALUES (:phone, :userName, :password, :role)", sqlParameterSource);
    }

    @Override
    public void deleteUserById(long id) {
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        jdbcTemplate.update("DELETE FROM mydb.users WHERE idusers = ?", id);
    }

    @Override
    public void deleteUserByPhone(String phone) {
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        jdbcTemplate.update("DELETE FROM mydb.users WHERE phone = ?", phone);
    }

    @Override
    public void updateUser(User user) {
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        jdbcTemplate.update("UPDATE mydb.users SET username = ?, password = ?, role = ? WHERE phone = ?", new Object[]{user.getUsername(), user.getPassword(), user.getRole().toString().toUpperCase(), user.getPhone()});
    }

    @Override
    public List<User> getAllUsers() {
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        return jdbcTemplate.query("SELECT * FROM mydb.users", new UserMapper());
    }
}