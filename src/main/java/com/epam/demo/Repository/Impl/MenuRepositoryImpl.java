package com.epam.demo.Repository.Impl;

import com.epam.demo.DTO.Dish;
import com.epam.demo.Repository.MenuRepository;
import com.epam.demo.util.mappers.DishMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.util.List;

@Repository
public class MenuRepositoryImpl implements MenuRepository {

    @Autowired
    private DataSource dataSource;

    @Override
    public List<Dish> getAllDishesByOrderId(int id) {
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        return jdbcTemplate.query("SELECT * FROM mydb.dishes " +
                "INNER JOIN dishes_has_order ON dishes_has_order.order_idorder = ?", new Object[]{id}, new DishMapper());
    }

    @Override
    public Dish getDishById(long id) {
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        return jdbcTemplate.queryForObject("SELECT * FROM mydb.dishes WHERE iddish = ?", new Object[]{id}, new DishMapper());
    }

    @Override
    public void addDish(Dish dish) {
        NamedParameterJdbcTemplate namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
        SqlParameterSource sqlParameterSource = new MapSqlParameterSource()
                .addValue("nameDish", dish.getDishname())
                .addValue("priceDish", dish.getPricedish());
        namedParameterJdbcTemplate.update("INSERT INTO mydb.dishes (dishName, dishPrice) VALUES (:nameDish, :priceDish)", sqlParameterSource);
    }



    @Override
    public void deleteDishById(long id) {
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        jdbcTemplate.update("DELETE mydb.dishes WHERE iddish = ?", id);
    }

    @Override
    public void deleteDishByName(String dishName) {
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        jdbcTemplate.update("DELETE mydb.dishes WHERE dishName = ?", dishName);
    }

    @Override
    public void updateDish(Dish dish) {
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        jdbcTemplate.update("UPDATE mydb.dishes SET dishName = ?, dishPrice = ? WHERE iddish = ?", new Object[]{dish.getDishname(), dish.getPricedish(), dish.getId()});
    }

    @Override
    public List<Dish> getAllDishes() {
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        return jdbcTemplate.query("SELECT * FROM mydb.dishes", new DishMapper());
    }

}
