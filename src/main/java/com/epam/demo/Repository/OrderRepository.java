package com.epam.demo.Repository;

import com.epam.demo.DTO.Order;

import java.util.ArrayList;
import java.util.List;

public interface OrderRepository {

    public Order getOrderById(long id);

    public List<Order> getOrderByIdUser(long id);

    public void createOrder(Order order, ArrayList<String> arrayList, int id);

    public void deleteOrderById(long id);

    public void deleteOrderByStatus(boolean status);

    public void updateOrder(Order order);

    public List<Order> getAllOrders();
}
