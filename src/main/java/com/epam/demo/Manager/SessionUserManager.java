package com.epam.demo.Manager;

import com.epam.demo.DTO.User;

public interface SessionUserManager {

    public void setCurrentSessionUser(User user);

    public User getCurrentSessionUser();
}
