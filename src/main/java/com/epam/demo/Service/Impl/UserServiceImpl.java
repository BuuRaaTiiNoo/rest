package com.epam.demo.Service.Impl;

import com.epam.demo.DTO.User;
import com.epam.demo.Repository.Impl.UserRepositoryImpl;
import com.epam.demo.Service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService{

    private final UserRepositoryImpl usersRepository;

    @Autowired
    public UserServiceImpl(UserRepositoryImpl usersRepository) {
        this.usersRepository = usersRepository;
    }

    @Override
    public void addUser(User user){
        usersRepository.addUser(user);
    }

    @Override
    public User authenticateUser(User user) {
        User foundUser = usersRepository.getUserByPhone(user.getPhone());

        if (!foundUser.getPassword().equals(user.getPassword())) {
            return null;
        }

        return foundUser;
    }

}
