package com.epam.demo.Service.Impl;

import com.epam.demo.DTO.Dish;
import com.epam.demo.Repository.Impl.MenuRepositoryImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MenuServiceImpl {

    private final MenuRepositoryImpl menuRepository;

    @Autowired
    public MenuServiceImpl(MenuRepositoryImpl menuRepository) {
        this.menuRepository = menuRepository;
    }


    public List<Dish> getAllDishesByOrderId(int id){
        return menuRepository.getAllDishesByOrderId(id);
    }

    public Dish getDishById(long id){
        return menuRepository.getDishById(id);
    }

    public List<Dish> getAllDishes() {
        return menuRepository.getAllDishes();
    }

    public MenuRepositoryImpl getMenuRepository() {
        return menuRepository;
    }
}
