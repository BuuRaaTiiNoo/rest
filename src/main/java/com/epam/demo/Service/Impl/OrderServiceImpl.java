package com.epam.demo.Service.Impl;

import com.epam.demo.DTO.Order;
import com.epam.demo.Repository.Impl.OrderRepositoryImpl;
import com.epam.demo.Service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.List;

@Service
public class OrderServiceImpl implements OrderService {

    @Autowired
    OrderRepositoryImpl orderRepository;

    @Override
    public Order getOrderById(long id) {
        return orderRepository.getOrderById(id);
    }

    @Override
    public List<Order> getOrderByIdUser(long id) {
        return orderRepository.getOrderByIdUser(id);
    }

    @Override
    public void createOrder(Order order, ArrayList<String> arrayList, int id) {
        orderRepository.createOrder(order, arrayList, id);
    }

    @Override
    public void confirmOrder(Order order) {
        orderRepository.updateOrder(order);
    }

    @Override
    public void payOrder(Order order) {
        orderRepository.updateOrder(order);
    }

    @Override
    public void closeOrder(Order order) {

    }

    @Override
    public List<Order> getAllOrders() {
        return orderRepository.getAllOrders();
    }
}
