package com.epam.demo.Service;

import com.epam.demo.DTO.User;

public interface UserService {

    public void addUser(User user);

    public User authenticateUser(User user);
}
