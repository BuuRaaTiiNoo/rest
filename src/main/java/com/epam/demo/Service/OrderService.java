package com.epam.demo.Service;

import com.epam.demo.DTO.Order;

import java.util.ArrayList;
import java.util.List;

public interface OrderService {

    public Order getOrderById(long id);

    public List<Order> getOrderByIdUser(long id);

    public void createOrder(Order order, ArrayList<String> arrayList, int id);

    public void confirmOrder(Order order);

    public void payOrder(Order order);

    public void closeOrder(Order order);

    public List<Order> getAllOrders();

}
