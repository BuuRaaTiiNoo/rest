<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@include file="top.jsp"%>
<div class="row">
    <div class="info">
        <form action="registration" method="post">
            <label>Login</label>
            <input type="text" id="username" name="username" placeholder="Your name..">

            <label>Phone Number</label>
            <input type="text" id="phone" name="phone" value="" placeholder="Your phone number..">

            <label>Password</label>
            <input type="password" id="password" name="password" placeholder="Your password..">

            <input id="btnReg" type="submit" value="Submit">
        </form>
        <c:forEach items="${errors}" var="error">
            ${error.defaultMessage}<br>
        </c:forEach>
    </div>
</div>
<%@include file="bottom.jsp"%>


