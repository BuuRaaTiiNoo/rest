<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@include file="top.jsp" %>
<div class="row">
    <table>
        <tr>
            <th>Статус оплаты заказа</th>
            <th>Стоимость заказа</th>
        </tr>
        <c:forEach items="${myorder}" var="myorder">
            <c:if test="${myorder.statMake==true}">
                <c:if test="${myorder.statPay==false}">
                    <tr>
                        <td>
                            Неоплачен
                        </td>
                        <td>${myorder.costOrder}</td>
                        <td>
                            <form action="myorder/${myorder.id}" method="post">
                                <input id="btn" name="btn" type="submit" value="pay">
                            </form>
                        </td>
                    </tr>
                </c:if>
            </c:if>
        </c:forEach>
    </table>
</div>
<%@include file="bottom.jsp" %>


