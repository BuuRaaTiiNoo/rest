<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@include file="top.jsp"%>
<div class="row">
    <table>
        <tr>
            <th>Название блюда</th>
            <th>Цена</th>
            <c:if test="${user.username!=null}">
                <th>Добавить</th>
            </c:if>
        </tr>
        <form action="menu" method="post">
            <c:forEach items="${menu}" var="dish">
                <tr>
                    <td>${dish.dishname}</td>
                    <td>${dish.pricedish}</td>
                    <c:if test="${user.username!=null}">
                        <td><input id="checkBox" name = "checkbox" type="checkbox" value="${dish.id}"></td>
                    </c:if>
                </tr>
            </c:forEach>
            <a href="/admin/myorder">
                <input id="btnReg" type="submit" value="Submit">
            </a>
        </form>
    </table>
</div>
<%@include file="bottom.jsp"%>


