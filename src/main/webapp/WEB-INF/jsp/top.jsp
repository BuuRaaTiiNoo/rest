<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<html>
<head>
    <title>NoNaMe</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body>
<style>
    <%@include file="../css/style.css" %>
</style>
<div class="column side"></div>
<div class="column middle">
    <div class="header">
        <h1>Ресторан NoNaMe</h1>
        <p>Некий интеренет - ресторан</p>
    </div>
    <ul id="navbar">
        <li><a href="/">Главная</a></li>
        <li><a href="menu">Меню</a></li>
        <li><a href="bucket">Корзина</a></li>
        <li><a href="about">О нас</a></li>
        <c:if test="${user.role=='ADMIN'}">
            <li><a href="admin">Адм.панель</a></li>
        </c:if>
        <c:if test="${user.username==null}">
            <li style="float:right"><a href="login">Войти</a></li>
        </c:if>
        <c:if test="${user.username!=null}">
            <li style="float:right"><a href="">${user.username}</a>
                <ul>
                    <li><a href="person">Мой профиль</a></li>
                    <li><a href="myorder">Мои заказы</a></li>
                    <li><a href="logout">Выход</a></li>
                </ul>
            </li>
        </c:if>
    </ul>