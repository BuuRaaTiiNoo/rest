<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@include file="top.jsp" %>
<div class="row">
    <table>
        <tr>
            <th>Список активных заказов</th>
            <th>Статус заказа</th>
            <th>Стоимость заказа</th>
            <th>Подтвердить</th>
        </tr>

        <c:forEach items="${orders}" var="orders">
            <c:if test="${orders.statPay==false}">
                <tr>
                    <td>
                        ${orders.id}
                    </td>
                    <td>${orders.statMake}</td>
                    <td>${orders.costOrder}</td>
                    <td>
                        <form action="admin/${orders.id}" method="post">
                            <input id="btn" name="btn" type="submit" value="confirm">
                        </form>
                    </td>
                </tr>
            </c:if>
        </c:forEach>

    </table>
</div>
<%@include file="bottom.jsp" %>


